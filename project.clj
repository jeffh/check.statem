(defproject net.jeffhui/check.statem "1.0.0-SNAPSHOT"
  :description "Provides ability to test using a state machine as a specification"
  :url "https://github.com/jeffh/check.statem"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.0-RC3"]
                 [org.clojure/test.check "0.10.0-alpha3"]])
